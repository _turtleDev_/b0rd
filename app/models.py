from django.db import models
from django.contrib.auth.models import User

class Bork(models.Model):
    timestamp = models.DateTimeField()
    user = models.ForeignKey(User)
    message = models.CharField(max_length=140)
