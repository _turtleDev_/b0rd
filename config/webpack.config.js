'use strict';

const path = require('path');
const AssetManifest = require('./assets.json');

const appRoot = path.resolve(path.join(__dirname, '..'));

module.exports = {
    context: appRoot,
    entry: AssetManifest.input.scripts,
    output: {
        path: path.resolve(AssetManifest.output.scripts),
        filename: '[name].bundle.js'
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['es2015', 'react']
                }
            }
        }]
    }
};
