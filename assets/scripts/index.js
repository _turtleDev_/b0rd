import React from 'react';
import ReactDOM from 'react-dom';

import App from './app/main.jsx';

ReactDOM.render(
    React.createElement(App),
    document.querySelector('#app')
);

