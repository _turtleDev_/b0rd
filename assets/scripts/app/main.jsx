'use strict';

import React from 'react';
import {
    BrowserRouter as Router, 
    Route, 
    Switch
} from 'react-router-dom';

import { ProtectedRoute, ProtectedComponent } from './protected.jsx';
import { About, NotFound, Splash, Loader } from './static.jsx';
import { Login, Logout, SignUp } from './auth.jsx';
import { Profile } from './profile.jsx';
import { Home } from './home.jsx';
import { Header } from './nav.jsx';
import request from './request.js';


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            auth: null,
            ready: false
        }
        this.init();
    }

    init() {
        request('GET', '/v0/me/')
        .then(JSON.parse.bind(JSON))
        .then((user) => {
            this.setState({ 
                auth: user,
                ready: true
            });
        })
        .catch((e) => {
            this.setState({
                ready: true
            });
        })

    }

    render() {

        if ( !this.state.ready ) {
            return <Loader />;
        }

        return (
            <Router>
                <div>
                    <Header auth={this.state.auth}/>
                    <div className='container-wide'>
                        <Switch>
                            <ProtectedRoute 
                                exact 
                                path="/" 
                                component={Home} 
                                auth={this.state.auth} 
                                redirectTo='/splash'
                            />
                            <ProtectedRoute 
                                path="/login" 
                                component={Login}
                                auth={this.state.auth} 
                                onSuccess={(user) => this.setState({auth: user})}
                                redirectTo='/login'
                            />
                            <ProtectedRoute 
                                path="/signup"
                                component={SignUp}
                                auth={this.state.auth}
                                onSuccess={(user) => this.setState({auth: user})}
                                redirectTo='/login'
                            />
                            <ProtectedRoute 
                                path="/logout"
                                component={Logout}
                                auth={this.state.auth}
                                onSuccess={() => this.setState({auth: null})}
                                redirectTo='/login'
                            />
                            <Route path="/profile/:id" component={Profile}/>
                            <Route path="/splash" component={Splash}/>
                            <Route path="/about" component={About}/>
                            <Route component={NotFound} />
                        </Switch>
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;
