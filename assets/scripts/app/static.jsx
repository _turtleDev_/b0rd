'use strict';

import React from 'react';
import { Link } from 'react-router-dom';

class About extends React.Component {
    render() {
        return (
            <div className="card">
                <h1>
                    <code>B0rd</code>
                </h1>
                <p>A (work in progress) twitter clone written using react and django</p>
                <p>Things are still in early stage of development, and for now very few features have been implemented</p>
                <p>you can find the source code <a href="https://bitbucket.org/_turtleDev_/b0rd">here</a></p>
                <p>License: MIT</p>
                <p>Dog Icon by <a href="https://thenounproject.com/Luis/">Luis Prado</a></p>
            </div>
        );
    }
}

class NotFound extends React.Component {
    render() {
        return (
            <div className="card">
                <h1>
                    <code>._. hey?</code>
                </h1>
                <p>... what are you doing here?</p>
                <p><Link to="/">home</Link></p>
            </div>
        );
    }
}

class Splash extends React.Component {
    render() {
        return (
            <div className="card">
                <h1>Welcome to the yet another twitter clone</h1>
                <p><Link to="/login">login</Link> or <Link to="/signup">signup</Link> to begin.</p>
            </div>
        );
    }
}

class Loader extends React.Component {
    render() {
        return (
            <div className="flex-column center-align center-justify">
                <p>
                    <span className="loader"></span>
                </p>
                <code>loading</code>
            </div>
        );
    }
}

export { Splash, About, NotFound, Loader };
