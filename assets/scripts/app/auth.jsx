'use strict';

import React from 'react';
import request from './request.js';
import { Link } from 'react-router-dom';

class AuthFormBase extends React.Component {
    /**
     * expects children to define a target on self
     */

    constructor(props) {

        super(props);

        this.state = {
            message: '',
            username: '',
            password: ''
        };
    }

    componentWillMount() {

        if ( !this.target ) {
            throw new Error(`${this.constructor.name} didn't define this.target`);
        }

        if ( this.props.auth ) {
            const to = (this.props.location.state || { from: '/' }).from;
            this.props.history.replace(to);
        }
    }

    handleChange(event) {
        switch(event.target.name) {
            case 'username':
                this.setState({username: event.target.value})
                break;
            case 'password':
                this.setState({password: event.target.value})
                break;
        }
    }

    handleSubmit(event) {

        event.preventDefault();

        const payload = {
            username: this.state.username,
            password: this.state.password
        };
        request('POST', this.target, payload)
        .then(JSON.parse.bind(JSON))
        .then((user) => {
            this.props.onSuccess(user);
            const to = (this.props.location.state || { from: '/' }).from;
            this.props.history.replace(to);
        })
        .catch((e) => {
            this.setState({ message: 'something went wrong' });
        });
    }
}

class Login extends AuthFormBase {

    constructor(props) {
        super(props);
        this.target = '/v0/auth/login/'
    }

    render() {
        return (
            <div className="grid">
                <div className="row container-narrow card">
                    <form className="col-6" onSubmit={this.handleSubmit.bind(this)}>
                        <h2>Login | b0rd</h2>
                        <div>
                        {!!this.state.message && 
                            <code>{this.state.message}</code>
                        }
                        </div>
                        <p>
                            <input
                                type="text" 
                                placeholder="username"
                                name="username"
                                value={this.state.username}
                                onChange={(e) => this.handleChange(e)}
                            />
                        </p>
                        <p>
                            <input
                                type="password"
                                placeholder="password"
                                name="password"
                                value={this.state.password}
                                onChange={(e) => this.handleChange(e)}
                            />
                        </p>
                        <p>
                            <input type="submit" />
                        </p>
                        <p>Don't have an account? <Link to="/signup">signup</Link></p>
                    </form>
                    <div className="col-6 flex-column">
                        <h2>Login using your social accounts</h2>
                        <p>
                            <button className="login login-facebook">Facebook</button>
                        </p>
                        <p>
                            <button className="login login-google">Google</button>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

class SignUp extends AuthFormBase {

    constructor(props) {
        super(props);
        this.target = '/v0/auth/signup/'
    }

    render() {
        return (
            <div className="grid">
                <div className="row container-narrow card">
                    <form className="col-6" onSubmit={this.handleSubmit.bind(this)}>
                        <h2>Signup | b0rd</h2>
                        <p>
                            <input
                                type="text" 
                                placeholder="username"
                                name="username"
                                value={this.state.username}
                                onChange={(e) => this.handleChange(e)}
                            />
                        </p>
                        <p>
                            <input
                                type="password"
                                placeholder="password"
                                name="password"
                                value={this.state.password}
                                onChange={(e) => this.handleChange(e)}
                            />
                        </p>
                        <p>
                            <input type="submit" />
                        </p>
                        <p> Already have an account? <Link to="/login">login</Link></p>
                    </form>
                    <div className="col-6">
                        <h2>Signup using your social accounts</h2>
                        <p>
                            <button className="login login-facebook">Facebook</button>
                        </p>
                        <p>
                            <button className="login login-google">Google</button>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

class Logout extends React.Component {

    componentWillMount() {
        const cb = () => {
            this.props.onSuccess();
            this.props.history.replace('/');
        }
        request('GET', '/v0/auth/logout/')
        .then(cb)
        .catch(cb);
    }

    render() {
        return (
            <code>hold tight ...</code>
        )
    }
}

export { Login, Logout, SignUp };
