'use strict';

import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { objectOrNull } from './props';

class ProtectedRoute extends React.Component {
    render() {
        const { path, exact, component: Comp } = this.props;
        return (
            <Route exact={!!exact} path={path} render={(routeProps) => (
                <Comp {...routeProps} {...this.props} />
            )}/>
        )
    }
}

ProtectedRoute.propTypes = {
    auth: objectOrNull,
    component: PropTypes.func.isRequired,
    redirectTo: PropTypes.string.isRequired
};

class ProtectedComponent extends React.Component {
    /**
     * XXX: be sure to call this method in any child classes
     * via super.componentWillMount()
     */
    componentWillMount(props) {
        if ( !this.props.auth ) {
            this.props.history.replace(this.props.redirectTo, {
                from: this.props.location.pathname
            });
        }
    }
}

export { ProtectedComponent, ProtectedRoute };
