'use strict';

import React from 'react';
import request from './request.js';
import { BorkList } from './home.jsx';
import { Loader } from './static.jsx';

class UserProfile extends React.Component {
    render() {
        return (
            <div className="profile profile-wide card">
                <div className="flex-column center-align">
                    <div className="image-placeholder image-128x128">
                        {this.props.user}
                    </div>
                    <h1 className="profile-heading">{this.props.user}</h1>
                </div>
            </div>
        );
    }
}

class Profile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            user: null,
            borks: []
        }
        this.load(props.match.params.id)
    }

    load(id) {

        /**
         * fire off both requests, and render the UI when the
         * user data becomes available.
         */
        request('GET', `/v0/users?user=${id}`)
        .then(JSON.parse.bind(JSON))
        .then((user) => this.setState({ ready: true, user }));


        request('GET', `/v0/bork?user=${id}`)
        .then(JSON.parse.bind(JSON))
        .then((borks) => this.setState({ borks }));
    }

    render() {

        if ( !this.state.ready ) {
            return <Loader />
        }

        return (
            <div className="flex-column center-align">
                <UserProfile user={this.state.user.user} />
                <BorkList borks={this.state.borks} />
            </div>
        );
    }
}

export { Profile };
