'use strict';

function Request(method, url, payload = null) {

    return new Promise((resolve, reject) => {

        const xhr = new XMLHttpRequest();

        xhr.open(method, url);

        xhr.onload = () => {
            if ( xhr.status >= 200 && xhr.status < 300 ) {
                return resolve(xhr.responseText);
            } else {
                return reject(xhr);
            }
        }

        xhr.onerror = () => {
            reject(xhr);
        }

        if ( payload && typeof payload === 'object' ) {
            payload = JSON.stringify(payload);
            xhr.setRequestHeader('Content-Type', 'application/json');
        }

        return xhr.send(payload);
    });
}

export default Request;
