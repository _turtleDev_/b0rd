'use strict';

import React from 'react';
import Moment from 'moment';
import request from './request.js';
import { ProtectedComponent } from './protected.jsx';
import { Link } from 'react-router-dom';

class BorkControl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
    }

    handleChange(event) {
        if ( event.target.value.length > 140 ) {
            return;
        }
        this.setState({
            value: event.target.value
        })
    }

    handleSubmit(event) {

        event.preventDefault();

        if ( this.state.value.length == 0 ) {
            return;
        }

        const payload = {
            message: this.state.value
        }

        request('POST', '/v0/bork_new/', payload)
        .then(() => {
            this.setState({ value: '' });
            this.props.refresh();
        });
    }

    render() {
        return (
            <form className="card flex-column bork-control" onSubmit={this.handleSubmit.bind(this)}>
                <textarea
                    onChange={this.handleChange.bind(this)}
                    value={this.state.value} 
                    placeholder={`What's happening ${this.props.user || ''}`}
                />
                <br/>
                <input type='submit'/>
            </form>
        );
    }
}

class BorkList extends React.Component {
    render() {
        return (
            <ul className="bork-list">
                {this.props.borks.map((bork) => (
                    <li className="media card bork" key={bork.id}>
                        <div className="media-left">
                            <div className="image-placeholder image-64x64">
                                {bork.user.username}
                            </div>
                        </div>
                        <div className="media-content">
                            <span>
                                <strong>{bork.user.username}</strong>
                                &nbsp;
                                <small>{Moment(bork.timestamp).fromNow()}</small>
                            </span>
                            <div>{bork.message}</div>
                            <div className="control">
                                <span className="typcn typcn-arrow-back" />
                                <span className="typcn typcn-arrow-repeat" />
                                <span className="typcn typcn-heart" />
                            </div>
                        </div>
                    </li>

                ))}
            </ul>
        )
    }
}

class ProfileCard extends React.Component {
    render() {
        const linkTo = `/profile/${this.props.user}`
        return (
            <div className="card">
                <Link to={linkTo}>
                    <div className="media">
                        <div className="media-left">
                            <div className="image-placeholder image-64x64">
                                {this.props.user}
                            </div>
                        </div>
                        <div className="media-content">
                            <strong>{this.props.user}</strong>
                        </div>
                    </div>
                </Link>
            </div>
        );
    }
}

class PromotionsCard extends React.Component {
    render() {
        return (
            <div className="card">
                <p>please spend more money on us. pls.</p>
                <p>one of our dev has broken all keys on his keyboard</p>
                <p>he has to use the mouse to write code</p>
                <p>have some mercy</p>
            </div>
        );
    }
}

class Home extends ProtectedComponent {
    constructor(props) {
        super(props);
        this.state = {
            borks: []
        };
        this.timer = null;
        this.load();
    }

    componentDidMount() {
        this.timer = setInterval(() => {
            this.load()
        }, 3000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
        this.timer = null;
    }

    load() {
        request('GET', '/v0/bork/')
        .then(JSON.parse.bind(JSON))
        .then((borks) => {
            if ( this.timer )
                this.setState({ borks })
        })
        .catch((e) => {/* do nothing */});
    }

    render() {
        let user = null;
        if (this.props.auth) {
            user = this.props.auth.user;
        }
        return (
            <div className="container grid">
                <div className="row">
                    <div className="col-3 col-collapse">
                        <ProfileCard user={user} />
                    </div>
                    <div className="col-6">
                        <BorkControl refresh={this.load.bind(this)}/>
                        <BorkList borks={this.state.borks}/>
                    </div>
                    <div className="col-3 col-collapse">
                        <PromotionsCard />
                    </div>
                </div>
            </div>
        );
    }
}

export { Home, BorkList };
