'use strict';

import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { objectOrNull } from './props';


class BurgerMenu extends React.Component {
    render() {
        let cls = ["nav-control"];
        if ( this.props.active ) 
            cls.push("nav-engaged");
        cls = cls.join(" ");
        
        return (
            <div className={cls} onClick={this.props.onClick}>
                <div className="bar"/>
                <div className="bar"/>
                <div className="bar"/>
            </div>
        );
    }
}

class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            active: false
        };
    }

    toggle() {
        this.setState((prevState) => (
            { active: !prevState.active } 
        ));
    }

    render() {

        const allRoutes = [
            {
                name: 'home',
                path: '/',
                activeClassName: 'nav-active'
            },
            {
                name: 'about',
                path: '/about',
                activeClassName: 'nav-active'
            },
            {
                name: 'profile',
                path: `/profile/${this.props.auth?this.props.auth.user:''}`,
                activeClassName: 'nav-active',
                authRequired: true
            },
            {
                name: 'logout',
                path: '/logout',
                className: 'nav-right',
                authRequired: true
            },
            {
                name: 'login',
                path: '/login',
                className: 'nav-right',
                activeClassName: 'nav-active',
                noAuthRequired: true
            }
        ];


        /**
         * a little explaination is in order.
         *
         * routes accept two flags:
         * authRequired: true | false,
         * noAuthRequired: true | false
         *
         * when authRequired is true, the element is shown only when logged in.
         * when noAuthRequired is true, the element is shown only when logeed out.
         */
        const routes = allRoutes
            .filter((x) => !x.authRequired || this.props.auth)
            .filter((x) => !x.noAuthRequired || !this.props.auth);

        let innerCls = ['nav-inner'];
        if ( this.state.active )
            innerCls.push('nav-engaged');
        innerCls = innerCls.join(' ');

        const toggle = this.toggle.bind(this);

        return (
            <header>
                <div className='nav'>
                    <NavLink to="/">
                        <img className="logo" src="/static/images/logo.svg"></img>
                    </NavLink>
                    <div className={innerCls}>
                        <ul className='nav-container'>
                            {routes.map((route) => (
                                <NavLink exact activeClassName={route.activeClassName} key={route.path} to={route.path} className={route.className}>
                                    <li onClick={toggle}>{route.name}</li>
                                </NavLink>
                            ))}
                        </ul>
                    </div>
                    <BurgerMenu 
                        active={this.state.active}
                        onClick={toggle}
                    />
                </div>
            </header>
        )

    }
}

Header.propTypes = {
    auth: objectOrNull
}

export { Header };
