'use strict';

function objectOrNull(props, propName, componentName) {
    if (typeof props[propName] !== 'object' ) {
        return new Error(`${componentName} expected property ${propName}`);
    }
}

export { objectOrNull };
