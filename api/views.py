import json
from django.shortcuts import render
from django.db.utils import IntegrityError
from app.models import Bork
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
from django.views.decorators.http import require_http_methods

# XXX: write tests for auth routes

def _user_details(user):
    return {
        'user': user.username,
        'id': user.id
    }

def auth_required(fn):
    def wrapper(request):
        if not request.user.is_authenticated:
            return HttpResponse(status=403)
        else:
            return fn(request)
    return wrapper

@auth_required
def ping(request):
    userdata = _user_details(request.user)
    return JsonResponse(userdata)

@require_http_methods(['POST'])
def user_login(request): 
    payload = json.loads(request.body.decode('utf-8'))
    username = payload['username']
    password = payload['password']

    user = authenticate(username=username, password=password)
    if user is None:
        return HttpResponse(status=400)
    else:
        login(request, user)
        userdata = _user_details(user)
        return JsonResponse(userdata)


@require_http_methods(['POST'])
def user_signup(request):
    payload = json.loads(request.body.decode('utf-8'))
    username = payload['username']
    password = payload['password']

    try:
        user = User.objects.create_user(username=username, password=password)
    except IntegrityError:
        payload = {
            'message': 'username already exists'
        }
        return JsonResponse(payload)

    login(request, user)
    userdata = _user_details(user)
    return JsonResponse(userdata)

def user_logout(request):
    logout(request)
    return HttpResponse(status=200)

@require_http_methods(['GET'])
def bork_list(request):
    transform = lambda x: {
        'id': x.id,
        'message': x.message,
        'timestamp': x.timestamp,
        'user': {
            'id': x.user.id,
            'username': x.user.username
        }
    }

    user = None
    try:
        user = request.GET['user']
    except:
        pass

    if user:
        try:
            query = User.objects.get(username=user).bork_set.order_by('-timestamp')
        except:
            return HttpResponse(status=400)

    else:
        query = Bork.objects.order_by('-timestamp')

    return JsonResponse([transform(b) for b in query], safe=False)

@auth_required
@require_http_methods(['POST'])
def bork_new(request):
    user = request.user
    payload = json.loads(request.body.decode('utf-8'))
    try:
        b = Bork(user=user, message=payload['message'], timestamp=timezone.now())
        b.save()
    except:
        return HttpResponse(status=500)

    # success
    return HttpResponse(status=200)

@require_http_methods(['GET'])
def view_user(request):
    try:
        user = request.GET['user']
    except:
        return HttpResponse(status=400)

    try:
        data = User.objects.get(username=user)
        data = _user_details(data)
    except:
        return HttpResponse(status=404)

    return JsonResponse(data)


