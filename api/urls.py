from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^me/$', view=views.ping, name='ping'),
    url(r'^auth/login/$', views.user_login, name='login'),
    url(r'^auth/logout/$', views.user_logout, name='logout'),
    url(r'^auth/signup/$', views.user_signup, name='signup'),
    url(r'^bork/$', views.bork_list, name='bork'),
    url(r'^bork_new/$', views.bork_new, name='bork_new'),
    url(r'^users/$', views.view_user, name='user')
]
