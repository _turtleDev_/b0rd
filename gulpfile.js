'use strict';

const Gulp = require('gulp');
const GulpSass = require('gulp-sass');
const GulpUtils = require('gulp-util');
const GulpAutoPrefixer = require('gulp-autoprefixer');
const Webpack = require('webpack');
const WebpackConfig = require('./config/webpack.config.js');
const AssetManifest = require('./config/assets.json');


const webpackCb = (err, stats) => {

    if ( err ) {
        throw new GulpUtils.PluginError('webpack', err);
    }

    GulpUtils.log(
        '[webpack]', 
        stats.toString({
            colors: true,
            chunks: false
        })
    );
};

Gulp.task('webpack:build', () => {
    Webpack(WebpackConfig).run(webpackCb);
});

Gulp.task('webpack:watch', () => {
    Webpack(WebpackConfig).watch({}, webpackCb);
});

Gulp.task('styles', () => {
    return Gulp.src(AssetManifest.input.styles)
        .pipe(GulpSass().on('error', GulpSass.logError))
        .pipe(GulpAutoPrefixer({ browsers: ['last 2 versions'], cascade: false }))
        .pipe(Gulp.dest(AssetManifest.output.styles));
});

Gulp.task('watch', ['styles', 'webpack:watch'], () => {
    Gulp.watch(AssetManifest.input.styles, ['styles']);
});

Gulp.task('build', ['styles', 'webpack:build']);
